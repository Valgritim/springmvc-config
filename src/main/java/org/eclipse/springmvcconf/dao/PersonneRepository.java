package org.eclipse.springmvcconf.dao;

import java.util.List;

import org.eclipse.springmvcconf.model.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository<Personne, Long>{
	List<Personne> findByNomAndPrenom(String nom,String prenom);
}
